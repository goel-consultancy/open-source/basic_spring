package consultancy.goels.code.basic.minimal.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Helloworld {

    @GetMapping("/show")
    public String getMessage(){
        return "{Hello World!!!}";
    }
}
