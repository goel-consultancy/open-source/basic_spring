package consultancy.goels.code.basic.minimal.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class minimalAPI {
    public static void main(String[] args){
        SpringApplication.run(minimalAPI.class, args);
    }
}
